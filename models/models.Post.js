const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const dotenv = require('dotenv');
dotenv.config();

const status = [process.env.TO_LEARN, process.env.LEARNING, process.env.LEARNED];

const PostSchema = new Schema({
    title: {
        type: String,
        require: true
    },
    description: {
        type: String,
    },
    status: {
        type: String,
        enum: status,
        default: status[0]
    },
    user: {
        type: Schema.Types.ObjectId,
        ref:'users'
    }
})

module.exports = mongoose.model('posts', PostSchema);