const express = require("express");
const route = require('./routes/routes.index');
const db = require('./config/db/dao');
const dotenv = require('dotenv');

dotenv.config()
const PORT = process.env.PORT || 3000

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));

route(app);

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`);
});
// npm start dev
