const Joi = require('joi')
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/models.user");
// dotenv.config()

const controllerAuth = {
    // [GET] /register
    // getRender(req, res){
    //     res.render('register')
    // }
    // [POST] /register
    async registerControl(req, res) {
        const { username, password, rePassword, email, first_name, last_name } = req.body;
        let errors = [];

        // Check username exist
        try {
            const user = await User.findOne({ username });
            if (user) {
                // errors.push({ msg: "Username is exist" });
                res.send({ msg: "Username is exist" });
            }
            else {
                // add user
                bcrypt.genSalt(10, function (err, salt) {
                    bcrypt.hash(password, salt, async function (err, hash) {
                        // Store hash in your password DB.
                        await User.create({
                            username: username,
                            password: hash,
                            email: email,
                            first_name,
                            last_name
                        });
                    });
                });
                res.send("Pass");
            }
        } catch (error) {
            console.log(error);
        }
    },

    async loginControl(req, res) {
        const { username, password } = req.body;

        try {
            const user = await User.findOne({ username });
            if (user) {
                const id = user._id
                const accessToken = jwt.sign(
                    { id },
                    process.env.ACCESS_TOKEN_SECRET,
                    { expiresIn: "1h" }
                );
                bcrypt.compare(password, user.password, function (err, result) {
                    if (result) {
                        res.json({ accessToken });
                        // res.send('Login success')
                    } else {
                        res.send("Wrong password");
                    }
                });
            } else {
                res.send("Username is not exist");
            }
        } catch (error) {
            console.log(error);
        }
    },

    // postControl(req, res) {
    //     const { username, password, rePassword } = req.body;
    //     res.send('Lets post')
    // },

    async authToken(req, res, next) {
        const authorizationHeader = req.headers["authorization"];
        // 'Beaer [token]'
        const token = authorizationHeader.split(" ")[1];
        if (!token) res.sendStatus(401);
        await jwt.verify(
            token,
            process.env.ACCESS_TOKEN_SECRET,
            (err, data) => {
                // console.log(err,data);
                // console.log(data.id);
                req.objectId = data.id
                if (err) res.sendStatus(403);
                next();
            }
        );
    },
};


module.exports = controllerAuth;
