const Post = require("../models/models.post");
const User = require("../models/models.user");
const controllerAuth = require("./controllers.auth");

const controllerPost = {
    async createPost(req, res) {
        const { title, description, status } = req.body;
        try {
            await Post.create({
                title,
                description,
                status,
                user: req.objectId,
            });
            res.send("create successfully");
        } catch (error) {
            console.log(error);
        }
    },

    async readPost(req, res) {
        try {
            const post = await Post.find({ user: req.objectId });
            if (post) {
                res.json(post);
            }
        } catch (error) {
            console.log(error);
        }
        // res.send('show success')
    },

    async readPostById(req, res) {
        try {
            const post = await Post.findOne({
                _id: req.params["id"],
                user: req.objectId,
            });
            if (post) {
                // console.log(post);
                res.json(post);
            }
        } catch (error) {
            console.log(error);
        }
    },

    // async readUpdatePost(req, res) {
    //     try {
    //         const post = await Post.findOne({
    //             _id: req.params["id"],
    //             user: req.objectId,
    //         });
    //         if (post) {
    //             // console.log(post);
    //             res.json(post);
    //         }
    //     } catch (error) {
    //         console.log(error);
    //     }
    // },

    async UpdatePost(req, res) {
        const { title, description, status } = req.body;
        try {
            await Post.updateOne(
                { _id: req.params["id"], user: req.objectId },
                {
                    title,
                    description,
                    status,
                }
            );
            res.send("update successfully");
        } catch (error) {
            console.log(error);
        }
    },

    async deletePost(req, res) {
        try {
            await Post.deleteOne({
                _id: req.params["id"], 
                user: req.objectId
            });
            res.send("delete successfully");
        } catch (error) {
            console.log(error);
        }
    },
};

module.exports = controllerPost;
