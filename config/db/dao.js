const dotenv = require('dotenv');
const mongoose = require('mongoose');

dotenv.config();

const MONGO_HOSTNAME = process.env.MONGO_HOSTNAME;
const MONGO_DB = process.env.MONGO_DB;

async function connect() {
  await mongoose.connect('mongodb://localhost:27017/MyProject_001')
    .then(() => console.log('MongoDB Connected...'))
    .catch(err => console.log(err))
  }
connect();

module.exports = { connect };
