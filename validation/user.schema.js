const Joi = require('joi');

const schema = {
  user: Joi.object({
    username: Joi.string().lowercase()
      .alphanum()
      .min(3)
      .max(30)
      .required(),
    password: Joi.string()
      .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).required(),
    rePassword: Joi.ref('password'),
    email: Joi.string().lowercase()
      .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }).required(),
    first_name: Joi.string().min(3).max(20).required(),
    last_name: Joi.string().min(1).max(20).required(),
    access_token: [
      Joi.string(),
      Joi.number()
    ]
  }).xor('password', 'access_token')
    .with('password', 'rePassword')
}

module.exports = schema;