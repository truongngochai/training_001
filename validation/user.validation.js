const { options } = require("joi");
const {user} = require("./user.schema")

module.exports = {
  addUserValidaiton: async (req, res, next) =>{
    const value = await user.validate(req.body,{abortEarly:false});
    if(value.error){
      const err = []
      value.error.details.forEach(detail => err.push(detail.message))
      err.join(', ')
      res.json({
        success: 0,
        message: err
      })
    }else{
      next();
    }
  }
}