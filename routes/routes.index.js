const auth = require("./routes.auth");
const post = require("./routes.post");

const controllerAuth = require('../controllers/controllers.auth');

function route(app) {
  app.use("/", auth);
  app.use("/post", controllerAuth.authToken, post);
}

module.exports = route;
