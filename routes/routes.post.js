const express = require('express');
const router = express.Router();

const controllerPost = require('../controllers/controllers.post');
router.get('/',controllerPost.readPost);
router.post('/create', controllerPost.createPost);
router.get('/detail/:id', controllerPost.readPostById);
// router.get('/update/:id', controllerPost.readUpdatePost);
router.put('/update/:id', controllerPost.UpdatePost);
router.post('/delete/:id', controllerPost.deletePost);

module.exports = router;