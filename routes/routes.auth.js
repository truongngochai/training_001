const express = require('express');
const { addUserValidaiton } = require("../validation/user.validation")

const router = express.Router();

const controllerAuth = require('../controllers/controllers.auth');

router.post('/register',addUserValidaiton ,controllerAuth.registerControl);
router.post('/login',controllerAuth.loginControl);
// router.post('/post', controllerAuth.authToken, controllerAuth.postControl);

module.exports = router;